import logo from './logo.svg';
import React from 'react';

import axios from 'axios';

import Recharts from 'recharts'
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from 'recharts';


export default class PersonList extends React.Component {

  state = {
    dataCovid: []
  }

  componentDidMount() {
    axios.get(`https://datacovidbackend.herokuapp.com/`)
      .then(res => {
        const info = res.data.data
        const arrayInfo = []
        info.map(i => {
          console.log(i)
          if(i.year<2021){
            return
          }
          if(i.month==1){
            if(i.day<17) return
          }
          let score=i.day+(i.month*30)+(i.year*365);
          const inf = {
            name: i.fulldate,
            vacina: i.value,
            score: score
          }
          arrayInfo.push(inf)
          return
        })
        const a = arrayInfo.sort((a, b) =>{
          
          return a.score - b.score
        })
        this.setState({ dataCovid: a });
        console.log(this.state)
      })
  }
  render() {
    return (
      <div>
        
        <center><h1>Dados da vacinação brasileira de COVID</h1>
      <LineChart
        width={1500}
        height={600}
        data={this.state.dataCovid}
        margin={{top: 5, right: 30, left: 20, bottom: 5}}
        >
        <Line
          type='monotone'
          dataKey='vacina'
          stroke='#8884d8'
          activeDot={{r: 10}}
          />
        <CartesianGrid strokeDasharray='10 10'/>
        <Tooltip/>
        <YAxis/>
        <XAxis dataKey='name'/>
        <Legend />
      </LineChart>
      <p> Última atualização 21/04</p>
      <p> Desenvolvido por <a href="https://www.linkedin.com/in/pedro-vallese/">Pedro Vallese</a></p></center>
      </div>
    )
  }
}